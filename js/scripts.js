function validateEmail(email) { 
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
} 
function validateForm1() {
     var box,
      isOK = true;
   $(".error").hide();
   $(".error-background").removeClass("error-background");
     

      if ($("#first-name").val() == "") {
          isOK = false;
          $("#first-name").addClass("error-background");
          $(".error-first-name").show();
      } 
      if ($("#last-name").val() == "") {
          isOK = false;
          $("#last-name").addClass("error-background");
          $(".error-last-name").show();
      }
      if ($("#title").val() == "") {
          isOK = false;
          $("#title").addClass("error-background");
          $(".error-title").show();
      }
      if ($("#company").val() == "") {
          isOK = false;
          $("#company").addClass("error-background");
          $(".error-company").show();
      }
      if ($("#email").val() == "") {
          isOK = false;
          $("#email").addClass("error-background");
          $(".error-email").show();
      } else if ( !validateEmail($("#email").val()) ) {
          isOK = false;
          $("#email").addClass("error-background");
          $(".error-email-valid").show();
      }

  return isOK;

}

$(function() {
/*
//Primary Nav
$(".secondary-nav .nav li, header .nav li").mouseenter( // top nav
function(){ 
	$(this).children(".subnav").css("min-width", $(this).width()); 
	$(this).children(".subnav2").css("min-width", '100'); 
	
	if($('html').hasClass('ie7')){  // check for IE7
		$(this).children(".subnav").css("width", $(this).width()); 
	}

	$(this).find(".middle").css("width", $(this).outerWidth() - 20 + "px"); 
	$(this).children(".subnav").stop(true, true).slideDown(200);
	$(this).children(".subnav2").stop(true, true).slideDown(200);
}
);
$(".secondary-nav .nav li, header .nav li").mouseleave( 
	function(){ 
		$(this).children(".subnav").hide(); // hide flyout
		$(this).children(".subnav2").hide(); // hide flyout
		//alert(document.location.hostname);
	}
);
//Primary Nav - Adding up pointer and adding rounded corners to lower than IE9
$(".nav .subnav li:last-child").after("<span class='pointer-up'></span><!--[if lt IE 9 ]><span class='rounded-corner left bottom'></span><span class='rounded-corner middle bottom'></span><span class='rounded-corner right bottom'></span><![endif]-->");
*/


//Trigger validation
$("#innovate-form button").click(function(event){
 
  if (validateForm1("#innovate-form")) {

  }
  else {
    event.preventDefault();
  }
});















   
});